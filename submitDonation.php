<?php
  include("config.php");
  include("session.php");
  $userDetails=$userClass->userDetails($session_id);
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MAGFest Charity</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/signin.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="/img/magfest.png" height=40px></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active"><a href="submitDonation.php">Submit Donation</a></li>
            <li><a href="viewDonations.php">View Donations</a></li>
            <li><a href="<?php echo BASE_URL; ?>logout.php"><?php echo $userDetails->username; ?> (Log Out)</a></li>
          </ul>
        </div><!--/nav-collapse -->

      </div>
    </nav>



    <div class="container">

      <div class="starter-template">
        

        <?php

          function testInput($data){
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
          } 

          $valid = 0;
          $amountErr = $emailErr = "";

          if ($_SERVER["REQUEST_METHOD"] == "POST"){
                    $regex = "/^[0-9]+(\.[0-9]{1,2})?$/";
                    if(empty($_POST["amount"]) || !preg_match($regex, $_POST["amount"])){
                        $amountErr = "A valid donation amount is required!";
                    }
                    else if($_POST["amount"] < 1.00){
                        $amountErr = "Minimum donation amount is $1!";
                    }
                    else{
                        $amount = testInput($_POST["amount"]);
                        $_SESSION["amount"] = number_format($amount,2,'.','');
                        $valid = 1;
                    }


                    if(empty($_POST["name"])){
                      $_SESSION["name"] = "Anonymous";
                    }
                    else{
                      $_SESSION["name"] = testInput($_POST["name"]);
                    }


                    if(empty($_POST["email"])){
                      $_SESSION["email"] = null;
                    }
                    else{
                      $_SESSION["email"] = testInput($_POST["email"]);
                    }

                    if(!empty($_SESSION["email"]) && !filter_var($_SESSION["email"], FILTER_VALIDATE_EMAIL)){
                        $emailErr = "Invalid email format!";
                        $valid = 0;
                    }


                    if(empty($_POST["incentive"])){
                      $_SESSION["incentive"] = null;
                    }
                    else{
                      $_SESSION["incentive"] = $_POST["incentive"];
                    }

                    if($_POST["event"] == "pachinko"){
                        $_SESSION["charity"] = "ablegamers";                        
                    }
                    else{
                        $_SESSION["charity"] = "childsplay";
                    }


                    $_SESSION["event"] = testInput($_POST["event"]);
                                                            

            }

            if($valid == 1){               
                    if(!headers_sent()){
                        header("Location: authDonation.php");
                    }
                    else{
                        ?>
                        <script type="text/javascript">
                        document.location.href="authDonation.php";
                        </script>
                        Thanks! One second, we're redirecting you...
                        <?php
                    }
                    exit();
                }
                else{ 


        ?>

        <div class="enter-donation">
          <h1>Enter Donation Total:</h1>
          <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" role="form">

          <div class="form-group">
            <span style="font-size:28pt;">$</span>
            <input type="text" name="amount" aria-label="Amount (to the nearest dollar)" placeholder="0.00" style="height:50px;width:350px;font-size:28pt;text-align:center;">

            <?php echo "<p style='color:red;'><b>".$amountErr."</b></p>"; //Display error if an amount was not provided. ?>
          </div>


          <?php
          switch($userDetails->role){
            case "admin":
            //Below is what the "admin" user sees
          ?>


          <div class="form-group">                    
              <h1>Choose an Event:<h1>
              
              <select name="event" style="height:50px;width:350px;font-size:18pt;text-align:center;">
                  <option value="tableflip">Table Flipping (Child's Play)</option>
                  <option value="promraffle">MAGProm Raffle (Child's Play)</option>
                  <option value="auction">Charity Auction (Child's Play)</option>
                  <option value="megamanathon">Mega Man-athon (Child's Play)</option>
                  <option value="pachinko">Pachinko (AbleGamers)</option>
              </select>
          </div>

          <hr>
          <p><b>Optional Fields Below</b></p>
          <hr>

          
          <h1>Enter Donator's Name:</h1>
          <div class="form-group">                
                <input type="text" name="name" aria-label="Name" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">                
          </div>

          <h1>Enter Donator's Email:</h1>
          <div class="form-group">                
                <input type="text" name="email" aria-label="Email" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">

                <?php echo "<p style='color:red;'><b>".$emailErr."</b></p>"; //Display error if an amount was not provided. ?>
          </div>   

          <?php
            break;

          case "megamanathon":
          //Below is what the "megamanathon" user sees
          ?>

          <hr>
          <p><b>Optional Fields Below</b></p>
          <p>Remind the donator they must give us at least an email to win a prize!</p>
          <hr>

          <h1>Enter Donator's Name:</h1>
          <div class="form-group">                
                <input type="text" name="name" aria-label="Name" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">                
          </div>       


          <h1>Enter Donator's Email:</h1>
          <div class="form-group">                
                <input type="text" name="email" aria-label="Email" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">

                <?php echo "<p style='color:red;'><b>".$emailErr."</b></p>"; //Display error if an amount was not provided. ?>
          </div>   


          <div class="form-group">                    
              <h1>Your Event:<h1>
              
              <select name="event" style="height:50px;width:350px;font-size:18pt;text-align:center;">
                  <option value="megamanathon">Mega Man-athon (Child's Play)</option>                  
              </select>
          </div>


          <?php 
            break;

          case "ablegamers":
          //Below is what the "ablegamers" user sees
          ?>         

          <hr>
          <p><b>Optional Fields Below</b></p>
          <hr>

          <h1>Enter Donator's Name:</h1>
          <div class="form-group">                
                <input type="text" name="name" aria-label="Name" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">                
          </div> 

          <h1>Enter Donator's Email:</h1>
          <div class="form-group">                
                <input type="text" name="email" aria-label="Email" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">

                <?php echo "<p style='color:red;'><b>".$emailErr."</b></p>"; //Display error if an amount was not provided. ?>
          </div>                   

          <div class="form-group">                    
              <h1>Your Event:<h1>
              
              <select name="event" style="height:50px;width:350px;font-size:18pt;text-align:center;">
                  <option value="pachinko">Pachinko (AbleGamers)</option>
              </select>
          </div>



          <?php
            break;

          case "tableflip":
          //Below is what the "tableflip" user sees
          ?>

          <hr>
          <p><b>Optional Fields Below</b></p>
          <hr>

          <h1>Enter Donator's Email:</h1>
          <div class="form-group">                
                <input type="text" name="email" aria-label="Email" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">

                <?php echo "<p style='color:red;'><b>".$emailErr."</b></p>"; //Display error if an amount was not provided. ?>
          </div>   

          <h1>Enter Donator's Name:</h1>
          <div class="form-group">                
                <input type="text" name="name" aria-label="Name" placeholder="" style="height:50px;width:350px;font-size:18pt;text-align:center;">                
          </div>       

          <div class="form-group">                    
              <h1>Your Charity:<h1>
              
              <select name="event" style="height:50px;width:350px;font-size:18pt;text-align:center;">
                  <option value="tableflip">Table Flip (Child's Play)</option>                  
              </select>
          </div>

          <?php
            break;
          }
          ?>

          <button type="submit" class="btn btn-primary">Submit</button>

          
          </form>
        </div>
      
      
    </div>

                <?php } //end else statement ?>

    </div><!-- /container -->

    
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
