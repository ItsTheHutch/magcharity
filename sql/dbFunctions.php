<?php
    
               
    function getDonationsTotal($scope){

        $sql = "SELECT TRUNCATE(SUM(Amount),2) FROM Transactions";

        if($scope == "childsplay"){
            $sql = "SELECT TRUNCATE(SUM(Amount),2) FROM Transactions WHERE Charity='childsplay'";
        }
        else if($scope == "ablegamers"){
            $sql = "SELECT TRUNCATE(SUM(Amount),2) FROM Transactions WHERE Charity='ablegamers'";
        }

        try {
            $db = getDB();
        
            
            //prepare sql
            $stmt = $db->prepare($sql);                         
            $stmt->execute();
            $total = $stmt->fetch();
            $currentTotal = $total[0];

            if($currentTotal == NULL){
                $currentTotal = 0.00;
            }
            
            return $currentTotal;
                        
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;
        
    }

    

    /**
     *  Submit the donation to the database.  Requiring this should work for both cash and online donations.
     *  POST data should be set to variables before using a require for this script.
     *
     *  @author Chris Hutcherson
     */
    
    function submitToDatabase($name, $email, $amount, $collectedBy, $charity, $event, $incentive, $message){


        try {
            $db = getDB();
        
            
            //prepare sql and bind parameters
            $stmt = $db->prepare("INSERT INTO Transactions (Name, Email, Message, Amount, CollectedBy, Charity, Event, Incentive) 
            VALUES (:name, :email, :message, :amount, :collectedBy, :charity, :event, :incentive)"); 
            
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':message', $message);
            $stmt->bindParam(':amount', $amount);
            $stmt->bindParam(':collectedBy', $collectedBy);
            $stmt->bindParam(':charity', $charity);
            $stmt->bindParam(':event', $event);
            $stmt->bindParam(':incentive', $incentive);
            
            $stmt->execute();
            
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;

    }




    /**
     *  View donations, most recent 50 submissions.
     *  POST data should be set to variables before using a require for this script.
     *
     * params: charity- name of charity viewed (or "all"), user- username is passed through, role- role/charity user belongs to
     *
     *  @author Chris Hutcherson
     */
    
    function viewDonations($charity,$user,$role){


        try {
            $db = getDB();

            $sql="";
        
            switch($charity){
                case "all":
                    $sql = "SELECT * FROM Transactions WHERE CollectedBy != 'web'";
                    break;
                case "megamanathon":
                    $sql = "SELECT * FROM Transactions WHERE CollectedBy != 'web' AND Charity = 'childsplay'";
                    break;
                case "tableflip":
                    $sql = "SELECT * FROM Transactions WHERE CollectedBy != 'web' AND Charity = 'childsplay'";
                    break;
                case "childsplay":
                    $sql = "SELECT * FROM Transactions WHERE CollectedBy != 'web' AND Charity = 'childsplay'";
                    break;
                case "ablegamers":
                    $sql = "SELECT * FROM Transactions WHERE CollectedBy != 'web' AND Charity = 'ablegamers'";
                    break;

            }

            if(strcmp($role,"admin") != 0){
                $sql .= " AND CollectedBy = '".$user."'";
            }            
            $sql .=" ORDER BY ID DESC LIMIT 50";

            //prepare sql and bind parameters
            $stmt = $db->prepare($sql);                     
            
            $stmt->execute();

            echo "<table class='table table-striped'><thead><tr><th>ID</th><th>Time</th><th>Name</th><th>Amount</th><th>Email</th><th>Collected By</th><th>Charity</th><th>Event</th><th>DELETE</th></tr></thead>";
            echo "<tbody>";

            while($row = $stmt->fetch()){          
                $id = $row["ID"];
                $timestamp = convertCharacters($row["TimeOfDonation"]);
                $name = convertCharacters($row["Name"]);
                $email = convertCharacters($row["Email"]);
                $amount = convertCharacters($row["Amount"]);
                $collectedBy = convertCharacters($row["CollectedBy"]);
                $donatedTo = convertCharacters($row["Charity"]);
                $event = convertCharacters($row["Event"]);
                echo "<tr><td>".$id."</td><td>".$timestamp."</td><td>".$name."</td><td>$".$amount."</td><td>".$email."</td><td>".$collectedBy."</td><td>".$donatedTo."</td><td>".$event."</td><td>[ <a href='removeDonation.php?id=".$id."' style='color:red;'>X</a> ]</td></tr>";
            }  

             echo "</tbody></table>";
            
            
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;

    }




    /**
     *  Remove donation.    
     *
     *  @author Chris Hutcherson
     */

    function removeDonation($id){
        try{
            $db = getDB();

            $stmt = $db->prepare("DELETE FROM Transactions WHERE ID = :id");
            $stmt->bindParam(':id', $id);

            $stmt->execute();
            
            
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;        
        
    }


    /**
     *  View a single donation.    
     *
     *  @author Chris Hutcherson
     */

    function getDonationById($id){
        try{
            $db = getDB();

            $stmt = $db->prepare("SELECT * FROM Transactions WHERE ID = :id");
            $stmt->bindParam(':id', $id);

            $stmt->execute();
            echo "<table class='table table-striped'><thead><tr><th>ID</th><th>Time</th><th>Name</th><th>Amount</th><th>Email</th><th>Collected By</th><th>Charity</th><th>Event</th></tr></thead>";
            echo "<tbody>";

            while($row = $stmt->fetch()){          
                $id = $row["ID"]; 
                $timestamp = convertCharacters($row["TimeOfDonation"]);  
                $name = convertCharacters($row["Name"]); 
                $email = convertCharacters($row["Email"]); 
                $amount = convertCharacters($row["Amount"]); 
                $collectedBy = convertCharacters($row["CollectedBy"]); 
                $donatedTo = convertCharacters($row["Charity"]); 
                $event = convertCharacters($row["Event"]);
                echo "<tr><td>".$id."</td><td>".$timestamp."</td><td>".$name."</td><td>$".$amount."</td><td>".$email."</td><td>".$collectedBy."</td><td>".$donatedTo."</td><td>".$event."</td></tr>";                
            }  

             echo "</tbody></table>";
            
            
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;        
        
    }



    /**
     *  Get the last 50 Childs Play donations.    
     *
     *  @author Chris Hutcherson
     */

    function getChildsPlayDonations(){
        try{
            $db = getDB();

            $stmt = $db->prepare("SELECT ID, TimeOfDonation, Name, Amount, Incentive, Message, Event FROM Transactions WHERE Charity = 'childsplay' ORDER BY ID DESC LIMIT 50");            

            $stmt->execute();
            echo "<div class='row'>
                    <div class='col-large-12 col-md-12 col-sm-12 col-xs-12'>
                    <table class='table table-striped table-responsive'>
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Time</th>
                    <th>Event</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Incentive</th>
                    <th>Message<th>
                    </tr>
                    </thead>";
            echo "<tbody>";

            while($row = $stmt->fetch()){          
                $id = $row["ID"]; 
                $timestamp = convertCharacters($row["TimeOfDonation"]);  
                $name = convertCharacters($row["Name"]);                
                $amount = convertCharacters($row["Amount"]); 
                $incentive = convertCharacters($row["Incentive"]);
                $message = convertCharacters($row["Message"]);
                $event = convertCharacters($row["Event"]);
                echo "<tr><td>".$id."</td><td>".$timestamp."</td><td>".$event."</td><td>".$name."</td><td>$".$amount."</td><td>".$incentive."</td><td>".$message."</td><</tr>";
            }  

             echo "</tbody></table></div></div>";
            
            
        }
        catch(PDOException $e){
            echo "Error: " . $e->getMessage();

        }           
        
        //End the database connection
        $conn = null;        
        
    }

    
?>