<?php
    class userClass{

        /* User Login */
        public function userLogin($username,$password){
            
            try{
                $db = getDB();
                $hash_password= hash('sha256', $password); //Password encryption 
                $stmt = $db->prepare("SELECT ID FROM Users WHERE Username=:username AND Password=:hash_password"); 
                $stmt->bindParam("username", $username,PDO::PARAM_STR) ;
                $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
                $stmt->execute();
                $count=$stmt->rowCount();
                $data=$stmt->fetch(PDO::FETCH_OBJ);
                $db = null;
                if($count){
                    $_SESSION['ID']=$data->ID; // Storing user session value
                    return true;
                }
                else{
                    return false;
                } 
            }
            catch(PDOException $e) {
                echo '{"error":{"text":'. $e->getMessage() .'}}';
            }

        }


        /* User Details */
        public function userDetails($uid){
            try{
                $db = getDB();
                $stmt = $db->prepare("SELECT username,role FROM Users WHERE ID=:uid"); 
                $stmt->bindParam("uid", $uid,PDO::PARAM_INT);
                $stmt->execute();
                $data = $stmt->fetch(PDO::FETCH_OBJ); //User data
                return $data;
            }
            catch(PDOException $e) {
                echo '{"error":{"text":'. $e->getMessage() .'}}';
            }
        }
        
    }
?>