<?php
  include("config.php");
  include("session.php");
  include("sql/dbFunctions.php");
  include("convertCharacters.php");
  $userDetails=$userClass->userDetails($session_id);

?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MAGFest Charity</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/signin.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="/img/magfest.png" height=40px></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active"><a href="submitDonation.php">Submit Donation</a></li>
            <li><a href="viewDonations.php">View Donations</a></li>
            <li><a href="<?php echo BASE_URL; ?>logout.php"><?php echo $userDetails->username; ?> (Log Out)</a></li>
          </ul>
        </div><!--/nav-collapse -->

      </div>
    </nav>



    <div class="container">

      <div class="starter-template">
        

        <?php

            $status = "";
            $submitComplete = 0;

            if($_SERVER["REQUEST_METHOD"] == "POST") {
                // username and password sent from form 
                
                $myusername = $userDetails->username;
                $mypassword = $_POST['password']; 
                
                $uid=$userClass->userLogin($myusername,$mypassword);
                if($uid){
                    removeDonation($_GET['id']);
                    $status = "<h1>Donation removed!</h1>";
                    $submitComplete = 1;
                }

                else {
                    $status = "Your Password is invalid. Please try again."; 
                }
            
            }
            
            if($submitComplete == 1){
                echo $status;
            }
            else{


        ?>

        
        <h3>You are about to remove the following donation:</h3>  

        <?php getDonationById($_GET['id']);?>
        
        <br>
        <h3>Enter your password below to confirm removal this donation:</h3>
        <form action="removeDonation.php?id=<?php echo $_GET['id']; ?>" method="post" role="form">

        <div class="form-group">
          
          <input type="password" name="password" aria-label="Password" placeholder="Password" style="height:50px;width:350px;font-size:28pt;text-align:center;">
          <?php echo "<p style='color:red;'><b>".$status."</b></p>"; //Display error if password was not provided. ?>
        </div>

        <button type="submit" class="btn btn-danger">Remove Donation</button>

        
        </form>
      
      
    </div>

                <?php } //end of else block ?>

    </div><!-- /container -->

    
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
